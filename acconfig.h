/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

#undef PACKAGE
#undef VERSION
#undef ENABLE_NLS
#undef HAVE_CATGETS
#undef HAVE_GETTEXT
#undef HAVE_LC_MESSAGES
#undef HAVE_STPCPY
#undef HAVE_GUILE

#undef GNOME_DEBUG_MAJOR_VERSION
#undef GNOME_DEBUG_MINOR_VERSION
#undef GNOME_DEBUG_MICRO_VERSION
#undef GNOME_DEBUG_VERSION
#undef GNOME_DEBUG_VERSION_CODE
#undef HAVE_GNOME_DEBUG
