/*  -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*-
 *
 * Copyright (C) 1999 Dave Camp <campd@oit.edu>, 
 *                    Martin Baulig <martin@home-of-linux.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <config.h>
#include <gnome.h>
#include <bonobo/bonobo.h>
#include <libgnorba/gnorba.h>

#include "gdf-program-events-object.h"
#include "gdf-program-events.h"

static void program_events_object_class_init (GdfProgramEventsObjectClass *class);
static void program_events_object_init (GdfProgramEventsObject *prog);
static void program_events_object_destroy (GdfProgramEventsObject *prog);
static void init_program_events_object_corba_class (void);

static GtkObjectClass *parent_class;

enum {
    EXECUTE_TIMEOUT,
    LAST_SIGNAL
};

static gint program_events_object_signals[LAST_SIGNAL] = {0};

struct _GdfProgramEventsObjectPrivate {
};

static POA_GDF_ProgramEvents__epv program_events_epv;
static POA_GDF_ProgramEvents__vepv program_events_vepv;

GdfProgramEventsObject *
gdf_program_events_object_new (void)
{
    GDF_ProgramEvents corba_object;
    GdfProgramEventsObject *object;

    object = gtk_type_new (gdf_program_events_object_get_type ());

    corba_object = gdf_program_events_object_corba_object_create (object);

    if (corba_object == CORBA_OBJECT_NIL){
	gtk_object_destroy (GTK_OBJECT (object));
	return NULL;
    }
	
    return gdf_program_events_object_construct (object, corba_object);
}

GDF_ProgramEvents
gdf_program_events_object_corba_object_create (GdfProgramEventsObject *object)
{
    POA_GDF_ProgramEvents *servant;
    CORBA_Environment ev;
	
    servant = (POA_GDF_ProgramEvents *)g_new0 (GnomeObjectServant, 1);
    servant->vepv = &program_events_vepv;

    CORBA_exception_init (&ev);

    POA_GDF_ProgramEvents__init ((PortableServer_Servant) servant, &ev);
    if (ev._major != CORBA_NO_EXCEPTION){
	g_free (servant);
	CORBA_exception_free (&ev);
	return CORBA_OBJECT_NIL;
    }
    CORBA_exception_free (&ev);

    return gnome_object_activate_servant (GNOME_OBJECT (object), servant);
}

GdfProgramEventsObject *
gdf_program_events_object_construct (GdfProgramEventsObject *object,
				     GDF_ProgramEvents corba_object)
{
    gnome_object_construct (GNOME_OBJECT (object), corba_object);

    return object;
}

GtkType 
gdf_program_events_object_get_type (void)
{
    static GtkType type = 0;
    
    if (!type) {
	GtkTypeInfo info = {
	    "Handle to a GNOME::Debugger::ProgramEvents object",
	    sizeof (GdfProgramEventsObject),
	    sizeof (GdfProgramEventsObjectClass),
	    (GtkClassInitFunc) program_events_object_class_init,
	    (GtkObjectInitFunc) program_events_object_init,
	    NULL,
	    NULL,
	    (GtkClassInitFunc) NULL
	};
	
	type = gtk_type_unique (gnome_object_get_type (), &info);
    }

    return type;
}

/* private routines */
static void
program_events_object_class_init (GdfProgramEventsObjectClass *klass) 
{
    GtkObjectClass *object_class = (GtkObjectClass*) klass;
    
    g_return_if_fail (klass != NULL);
    g_return_if_fail (GDF_IS_PROGRAM_EVENTS_OBJECT_CLASS (klass));
    
    parent_class = gtk_type_class (gnome_object_get_type ());

    program_events_object_signals [EXECUTE_TIMEOUT] =
	gtk_signal_new ("execute_timeout",
			GTK_RUN_LAST,
			object_class->type,
			GTK_SIGNAL_OFFSET (GdfProgramEventsObjectClass,
					   execute_timeout),
			gtk_marshal_NONE__INT,
			GTK_TYPE_NONE, 1,
			GTK_TYPE_INT);

    gtk_object_class_add_signals (object_class,
				  program_events_object_signals,
				  LAST_SIGNAL);
    object_class->destroy = (GtkSignalFunc) program_events_object_destroy;

    init_program_events_object_corba_class ();
}

static void
program_events_object_init (GdfProgramEventsObject *object)
{
    object->priv = NULL;
}

static void
program_events_object_destroy (GdfProgramEventsObject *object)
{
    if (parent_class->destroy)
	parent_class->destroy (GTK_OBJECT (object));
}

static void
impl_GDF_ProgramEvents_execute_timeout (PortableServer_Servant servant,
					const CORBA_long serial,
					CORBA_Environment *ev)
{
    GdfProgramEventsObject *object = GDF_PROGRAM_EVENTS_OBJECT
	(gnome_object_from_servant (servant));
    CORBA_Environment evx;

    CORBA_exception_init (&evx);

    GDF_TRACE_EXTRA ("object = %p, serial = %ld", object, serial);

    gtk_signal_emit (GTK_OBJECT (object),
		     program_events_object_signals [EXECUTE_TIMEOUT],
		     serial);

    CORBA_exception_free (&evx);
}

static void
init_program_events_object_corba_class (void) 
{
    /* EPV */
    program_events_epv.execute_timeout = &impl_GDF_ProgramEvents_execute_timeout;
    
    /* VEPV */
    program_events_vepv.GDF_ProgramEvents_epv = &program_events_epv;
}
