/*  -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*-
 *
 * Copyright (C) 1999 Dave Camp <campd@oit.edu>, 
 *                    Martin Baulig <martin@home-of-linux.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#ifndef __GDF_PROGRAM_EVENTS_CLIENT_H__
#define __GDF_PROGRAM_EVENTS_CLIENT_H__

BEGIN_GNOME_DECLS

#include <gdf/gdf.h>

typedef struct _GdfProgramEventsClient		GdfProgramEventsClient;
typedef struct _GdfProgramEventsClientPrivate	GdfProgramEventsClientPrivate;
typedef struct _GdfProgramEventsClientClass	GdfProgramEventsClientClass;

#define GDF_PROGRAM_EVENTS_CLIENT_TYPE		(gdf_program_events_client_get_type ())
#define GDF_PROGRAM_EVENTS_CLIENT(o)		(GTK_CHECK_CAST ((o), GDF_PROGRAM_EVENTS_CLIENT_TYPE, GdfProgramEventsClient))
#define GDF_PROGRAM_EVENTS_CLIENT_CLASS(k)	(GTK_CHECK_CLASS_CAST((k), GDF_PROGRAM_EVENTS_CLIENT_TYPE, GdfProgramEventsClientClass))
#define GDF_IS_PROGRAM_EVENTS_CLIENT(o)	(GTK_CHECK_TYPE ((o), GDF_PROGRAM_EVENTS_CLIENT_TYPE))
#define GDF_IS_PROGRAM_EVENTS_CLIENT_CLASS(k)	(GTK_CHECK_CLASS_TYPE ((k), GDF_PROGRAM_EVENTS_CLIENT_TYPE))

struct _GdfProgramEventsClient {
    GdfTypedEventInterface parent;

    GdfProgramEventsClientPrivate *priv;
};

struct _GdfProgramEventsClientClass {
    GdfTypedEventInterfaceClass parent_class;
};

extern GdfProgramEventsClient *
gdf_program_events_client_new (GdfTypedEventChannelClient *);

extern GdfProgramEventsClient *
gdf_program_events_client_construct (GdfProgramEventsClient *,
				     GdfTypedEventChannelClient *);

extern GtkType 
gdf_program_events_client_get_type (void);

extern void
gdf_program_events_client_execute_timeout (GdfProgramEventsClient *,
					   glong serial);

END_GNOME_DECLS

#endif
