/*  -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*-
 *
 * Copyright (C) 1999 Dave Camp <campd@oit.edu>, 
 *                    Martin Baulig <martin@home-of-linux.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <config.h>
#include <gnome.h>
#include <bonobo/bonobo.h>
#include <libgnorba/gnorba.h>
#include <gdf/gdf.h>

static CORBA_Object
debugger_program_events_activator (PortableServer_POA, const char *,
				   const char **, gpointer *,
				   CORBA_Environment *);

static void
debugger_program_events_deactivator (PortableServer_POA, const char *,
				     gpointer, CORBA_Environment *);

static const char *repo_id[] = {"IDL:GDF/ProgramEvents:1.0"};
static GnomePluginObject plugin_object[] =
{
    {
	repo_id,
	"gdf_program_events",
	NULL,
	"GDF Program Events interface",
	&debugger_program_events_activator,
	&debugger_program_events_deactivator
    },
    { NULL }
};

GnomePlugin GNOME_Plugin_info =
{
    plugin_object, NULL
};

static void
push_event_func (CORBA_any *any, gpointer user_data)
{
    GdfEventChannelClient *client = (GdfEventChannelClient*) user_data;

    GDF_TRACE_EXTRA ("any = %p, client = %p", any, client);

    gdf_event_channel_client_push (client, any);
}

static CORBA_Object
debugger_program_events_activator (PortableServer_POA poa,
				   const char *goad_id,
				   const char **params,
				   gpointer *impl_ptr,
				   CORBA_Environment *ev)
{
    CORBA_Object retval;
    CosEventChannelAdmin_EventChannel raw_event_channel;
    GdfEventChannelClient *client;

    GDF_TRACE_EXTRA ("params = %p", params);

    GDF_TRACE_EXTRA ("ior = '%s'", params [0]);

    raw_event_channel = CORBA_ORB_string_to_object (poa->orb, params [0], ev);

    GDF_TRACE_EXTRA ("raw_event_channel = %p", raw_event_channel);

    client = gdf_event_channel_client_new_from_corba (raw_event_channel);

    GDF_TRACE_EXTRA ("client = %p", client);

    retval = impl_GDF_ProgramEvents__create
	(poa, push_event_func, (gpointer) client, ev);

    return (CORBA_Object) retval;
}

static void
debugger_program_events_deactivator (PortableServer_POA poa,
				     const char *goad_id,
				     gpointer impl_ptr,
				     CORBA_Environment *ev)
{
}
