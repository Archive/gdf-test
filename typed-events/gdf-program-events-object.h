/*  -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*-
 *
 * Copyright (C) 1999 Dave Camp <campd@oit.edu>, 
 *                    Martin Baulig <martin@home-of-linux.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#ifndef __GDF_PROGRAM_EVENTS_OBJECT_H__
#define __GDF_PROGRAM_EVENTS_OBJECT_H__

BEGIN_GNOME_DECLS

#include <gdf/gdf.h>
#include "gdf-program-events.h"

typedef struct _GdfProgramEventsObject		GdfProgramEventsObject;
typedef struct _GdfProgramEventsObjectPrivate	GdfProgramEventsObjectPrivate;
typedef struct _GdfProgramEventsObjectClass	GdfProgramEventsObjectClass;

#define GDF_PROGRAM_EVENTS_OBJECT_TYPE		(gdf_program_events_object_get_type ())
#define GDF_PROGRAM_EVENTS_OBJECT(o)		(GTK_CHECK_CAST ((o), GDF_PROGRAM_EVENTS_OBJECT_TYPE, GdfProgramEventsObject))
#define GDF_PROGRAM_EVENTS_OBJECT_CLASS(k)	(GTK_CHECK_CLASS_CAST((k), GDF_PROGRAM_EVENTS_OBJECT_TYPE, GdfProgramEventsObjectClass))
#define GDF_IS_PROGRAM_EVENTS_OBJECT(o)	(GTK_CHECK_TYPE ((o), GDF_PROGRAM_EVENTS_OBJECT_TYPE))
#define GDF_IS_PROGRAM_EVENTS_OBJECT_CLASS(k)	(GTK_CHECK_CLASS_TYPE ((k), GDF_PROGRAM_EVENTS_OBJECT_TYPE))

struct _GdfProgramEventsObject {
    GnomeObject parent;

    GdfProgramEventsObjectPrivate *priv;
};

struct _GdfProgramEventsObjectClass {
    GnomeObjectClass parent_class;

    void (*execute_timeout) (long);
};

extern GdfProgramEventsObject *
gdf_program_events_object_new (void);

extern GdfProgramEventsObject *
gdf_program_events_object_construct (GdfProgramEventsObject *,
				     GDF_ProgramEvents);

extern GDF_ProgramEvents
gdf_program_events_object_corba_object_create (GdfProgramEventsObject *);

extern GtkType 
gdf_program_events_object_get_type (void);

END_GNOME_DECLS

#endif
