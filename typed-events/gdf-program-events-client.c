/*  -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*-
 *
 * Copyright (C) 1999 Dave Camp <campd@oit.edu>, 
 *                    Martin Baulig <martin@home-of-linux.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <config.h>
#include <gnome.h>
#include <bonobo/bonobo.h>
#include <libgnorba/gnorba.h>

#include "gdf-program-events-client.h"
#include "gdf-program-events.h"

static void program_events_client_class_init (GdfProgramEventsClientClass *class);
static void program_events_client_init (GdfProgramEventsClient *prog);
static void program_events_client_destroy (GdfProgramEventsClient *prog);

static GtkObjectClass *parent_class;

enum {
    LAST_SIGNAL
};

static gint program_events_client_signals[LAST_SIGNAL] = {0};

struct _GdfProgramEventsClientPrivate {
};

GdfProgramEventsClient *
gdf_program_events_client_new (GdfTypedEventChannelClient *client)
{
    GdfProgramEventsClient *interface;

    interface = gtk_type_new (gdf_program_events_client_get_type ());

    gdf_program_events_client_construct (interface, client);

    return interface;
}

GdfProgramEventsClient *
gdf_program_events_client_construct (GdfProgramEventsClient *interface,
				     GdfTypedEventChannelClient *client)
{
    gdf_typed_event_interface_construct
	(GDF_TYPED_EVENT_INTERFACE (interface), client,
	 "gdf_program_events");

    return interface;
}

GtkType 
gdf_program_events_client_get_type (void)
{
    static GtkType type = 0;
    
    if (!type) {
	GtkTypeInfo info = {
	    "Handle to a GNOME::Debugger::ProgramEvents object",
	    sizeof (GdfProgramEventsClient),
	    sizeof (GdfProgramEventsClientClass),
	    (GtkClassInitFunc) program_events_client_class_init,
	    (GtkObjectInitFunc) program_events_client_init,
	    NULL,
	    NULL,
	    (GtkClassInitFunc) NULL
	};
	
	type = gtk_type_unique (gdf_typed_event_interface_get_type (), &info);
    }

    return type;
}

/* private routines */
static void
program_events_client_class_init (GdfProgramEventsClientClass *klass) 
{
    GtkObjectClass *object_class = (GtkObjectClass*) klass;
    
    g_return_if_fail (klass != NULL);
    g_return_if_fail (GDF_IS_PROGRAM_EVENTS_CLIENT_CLASS (klass));
    
    parent_class = gtk_type_class (gdf_typed_event_interface_get_type ());

    gtk_object_class_add_signals (object_class,
				  program_events_client_signals,
				  LAST_SIGNAL);
    object_class->destroy = (GtkSignalFunc) program_events_client_destroy;
}

static void
program_events_client_init (GdfProgramEventsClient *client)
{
    client->priv = NULL;
}

static void
program_events_client_destroy (GdfProgramEventsClient *client)
{
    if (parent_class->destroy)
	parent_class->destroy (GTK_OBJECT (client));
}

void
gdf_program_events_client_execute_timeout (GdfProgramEventsClient *interface, glong serial)
{
    GDF_ProgramEvents objref;
    CORBA_Environment ev;

    CORBA_exception_init (&ev);

    objref = gnome_object_corba_objref (GNOME_OBJECT (interface));

    GDF_ProgramEvents_execute_timeout (objref, serial, &ev);

    CORBA_exception_free (&ev);
}
