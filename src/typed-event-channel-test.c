/*  -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*-
 *
 * Copyright (C) 1999 Dave Camp <campd@oit.edu>, 
 *                    Martin Baulig <martin@home-of-linux.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <config.h>
#include <gnome.h>

#include <signal.h>
#include <gdf/gdf.h>

#include "gdf-program-events-client.h"

int
main (int argc, char *argv[])
{
    CORBA_ORB orb;
    CORBA_Environment ev;
    GdfTypedEventChannelClient *channel;
    GdfProgramEventsClient *client;
    GdfSequencerClient *sequencer;
    CORBA_Object obj;
    char *ior;

    CORBA_exception_init (&ev);

    orb = gnome_CORBA_init ("typed-event-channel-test", VERSION,
			    &argc, argv, GNORBA_INIT_SERVER_FUNC, &ev);
	
    if (!bonobo_init (orb, NULL, NULL))
	g_error (_("Can't initialize bonobo!"));

    channel = gdf_typed_event_channel_client_new ();

    obj = gnome_object_corba_objref (GNOME_OBJECT (channel));
    ior = CORBA_ORB_object_to_string (orb, obj, &ev);

    printf ("%s\n", ior);
    fflush (NULL);

    GDF_TRACE_EXTRA ("channel = %p, obj = %p", channel, obj);

    client = gdf_program_events_client_new (channel);

    GDF_TRACE_EXTRA ("client = %p", client);

    sequencer = gdf_sequencer_client_new ();

    GDF_TRACE_EXTRA ("sequencer = %p", sequencer);

    for (;;) {
	long serial;
	CORBA_any any;

	/* So you get a chance to fire up the consumer. */
	raise (SIGSTOP);

	serial = gdf_sequencer_get_serial (sequencer);

	GDF_TRACE_EXTRA ("serial = %ld", serial);

	gdf_program_events_client_execute_timeout (client, serial);

	any._type = (CORBA_TypeCode) &TC_long_struct;
	any._value = &serial;
	CORBA_any_set_release (&any, CORBA_FALSE);

	GDF_TRACE_EXTRA ("any._type = '%s', any._value = %ld",
			 any._type->name, *(long*) any._value);

	gdf_event_channel_client_push
	    (GDF_EVENT_CHANNEL_CLIENT (channel), &any);
    }

    bonobo_main ();

    return 0;
}
