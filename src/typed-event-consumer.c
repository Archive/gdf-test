/*  -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*-
 *
 * Copyright (C) 1999 Dave Camp <campd@oit.edu>, 
 *                    Martin Baulig <martin@home-of-linux.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <config.h>
#include <gnome.h>

#include <ctype.h>
#include <signal.h>
#include <gdf/gdf.h>

static void
event_pushed_cb (GdfEventChannelClient *channel, CORBA_any *any)
{
    GDF_TRACE_EXTRA ("channel = %p, any = %p, any._type = '%s', any._value = %ld",
		     channel, any, any->_type->name, *(long*)any->_value);
}

int
main (int argc, char *argv[])
{
    CORBA_ORB orb;
    CORBA_Environment ev;
    GdfTypedEventChannelClient *channel;
    CORBA_Object obj;

    CORBA_exception_init (&ev);

    orb = gnome_CORBA_init ("typed-event-consumer", VERSION, &argc, argv,
			    GNORBA_INIT_SERVER_FUNC, &ev);
	
    if (!bonobo_init (orb, NULL, NULL))
	g_error (_("Can't initialize bonobo!"));

    if (argc == 1) {
	char *ior;

	channel = gdf_typed_event_channel_client_new ();

	obj = gnome_object_corba_objref (GNOME_OBJECT (channel));
	ior = CORBA_ORB_object_to_string (orb, obj, &ev);

	fprintf (stderr, "%s\n", ior);
	fflush (stderr);
    } else {
	char objref_str [2048];
	char *objref_str_ptr;
	int i;

	/* If there was an argument, use that as an IOR for an existing
	 * channel. */
	objref_str_ptr = argv [1];
        
        i = strlen (objref_str_ptr) - 1;
        while (isspace (objref_str_ptr [i]))
	    objref_str [i--] = '\0';
        g_assert (!(strlen (objref_str_ptr) % 2));
	obj = CORBA_ORB_string_to_object (orb, objref_str_ptr, &ev);
	
	channel = gdf_typed_event_channel_client_new_from_corba (obj);
    }

    GDF_TRACE_EXTRA ("channel = %p, obj = %p", channel, obj);

    gtk_signal_connect (GTK_OBJECT (channel), "event_pushed",
			event_pushed_cb, NULL);

    gdf_event_channel_client_listen (GDF_EVENT_CHANNEL_CLIENT (channel));

    GDF_TRACE ();

    bonobo_main ();

    return 0;
}
