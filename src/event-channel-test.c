/*  -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*-
 *
 * Copyright (C) 1999 Dave Camp <campd@oit.edu>, 
 *                    Martin Baulig <martin@home-of-linux.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <config.h>
#include <gnome.h>

#include <signal.h>
#include <gdf/gdf.h>

int
main (int argc, char *argv[])
{
    CORBA_ORB orb;
    CORBA_Environment ev;
    GdfEventChannelClient *channel;
    CORBA_Object obj;
    char *ior;

    CORBA_exception_init (&ev);

    orb = gnome_CORBA_init ("event-channel-test", VERSION, &argc, argv,
			    GNORBA_INIT_SERVER_FUNC, &ev);
	
    if (!bonobo_init (orb, NULL, NULL))
	g_error (_("Can't initialize bonobo!"));

    channel = gdf_event_channel_client_new ();

    obj = gnome_object_corba_objref (GNOME_OBJECT (channel));
    ior = CORBA_ORB_object_to_string (orb, obj, &ev);

    printf ("%s\n", ior);
    fflush (NULL);

    GDF_TRACE_EXTRA ("channel = %p, obj = %p", channel, obj);

    for (;;) {
	CORBA_any any;
	long test_value;

	time (&test_value);

	any._type = (CORBA_TypeCode) &TC_long_struct;
	any._value = &test_value;
	CORBA_any_set_release (&any, CORBA_FALSE);

	/* So you get a chance to fire up the consumer. */
	raise (SIGSTOP);

	GDF_TRACE_EXTRA ("any._type = '%s', any._value = %ld",
			 any._type->name, *(long*) any._value);

	gdf_event_channel_client_push (channel, &any);
    }

    bonobo_main ();

    return 0;
}
